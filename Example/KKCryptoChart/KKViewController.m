//
//  KKViewController.m
//  KKCryptoChart
//
//  Created by HenryDang on 02/07/2022.
//  Copyright (c) 2022 HenryDang. All rights reserved.
//

#import "KKViewController.h"
#import <KKCryptoChart/KKCryptoChart.h>
#import "KKSecondViewController.h"

@interface KKViewController ()

@end

@implementation KKViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Combined Chart";
    
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc] initWithTitle:@"jump" style:UIBarButtonItemStyleDone target:self action:@selector(btnClick)];
    self.navigationItem.rightBarButtonItem = rightItem;
    
    [self initChartUI];
}

- (void)btnClick
{
    KKSecondViewController *svc = [[KKSecondViewController alloc] init];
    [self.navigationController pushViewController:svc animated:YES];
}

- (void)initChartUI {
    KKCryptoChartView *cryptoChartView = [[KKCryptoChartView alloc] initWithFrame:CGRectMake(0, 104, self.view.frame.size.width, 500)];
    [self.view addSubview:cryptoChartView];
    KKCryptoChartGlobalConfig *config = [[KKCryptoChartGlobalConfig alloc] initWithLocale:@"hk" timeType:@"15" coinPrecision:@"2" tradeVolumePrecision:@"5"  environment:@"prod" coinCode:@"BTC_USDT"];//MATIC_USDT,FTM_USDT,DOSE_USDT
    cryptoChartView.config = config;
    
//    KKCryptoChartView *cryptoChartView1 = [[KKCryptoChartView alloc] initWithFrame:CGRectMake(5, 300 + 124, self.view.frame.size.width - 10, 300)];
//    [self.view addSubview:cryptoChartView1];
//    KKCryptoChartGlobalConfig *config1 = [[KKCryptoChartGlobalConfig alloc] initWithLocale:@"hk" timeType:@"15" coinPrecision:@"4" tradeVolumePrecision:@"5"  environment:@"prod" coinCode:@"FTM_USDT"];//MATIC_USDT,FTM_USDT,DOSE_USDT
//    cryptoChartView1.config = config1;
}


@end
