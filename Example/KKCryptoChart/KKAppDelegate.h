//
//  KKAppDelegate.h
//  KKCryptoChart
//
//  Created by HenryDang on 02/07/2022.
//  Copyright (c) 2022 HenryDang. All rights reserved.
//

@import UIKit;

@interface KKAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
