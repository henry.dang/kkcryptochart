//
//  KKSecondViewController.m
//  KKCryptoChart_Example
//
//  Created by 杨鹏 on 2022/6/28.
//  Copyright © 2022 HenryDang. All rights reserved.
//

#import "KKSecondViewController.h"
#import <KKCryptoChart/KKCryptoChart.h>

@interface KKSecondViewController ()

@end

@implementation KKSecondViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Second Chart";
    self.view.backgroundColor = [UIColor whiteColor];
    [self initChartUI];
}

- (void)initChartUI {
    KKCryptoChartView *cryptoChartView = [[KKCryptoChartView alloc] initWithFrame:CGRectMake(0, 104, self.view.frame.size.width, 500)];
    [self.view addSubview:cryptoChartView];
    KKCryptoChartGlobalConfig *config = [[KKCryptoChartGlobalConfig alloc] initWithLocale:@"hk" timeType:@"15" coinPrecision:@"8" tradeVolumePrecision:@"5"  environment:@"prod" coinCode:@"SHIB_USDT"];//MATIC_USDT,FTM_USDT,DOSE_USDT
    cryptoChartView.config = config;
    
//    KKCryptoChartView *cryptoChartView1 = [[KKCryptoChartView alloc] initWithFrame:CGRectMake(5, 300 + 124, self.view.frame.size.width - 10, 300)];
//    [self.view addSubview:cryptoChartView1];
//    KKCryptoChartGlobalConfig *config1 = [[KKCryptoChartGlobalConfig alloc] initWithLocale:@"hk" timeType:@"15" coinPrecision:@"4" tradeVolumePrecision:@"5"  environment:@"prod" coinCode:@"FTM_USDT"];//MATIC_USDT,FTM_USDT,DOSE_USDT
//    cryptoChartView1.config = config1;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
