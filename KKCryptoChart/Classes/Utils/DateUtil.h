//
//  DateUtil.h
//  KikiChartsDemo
//
//  Created by apple on 2021/3/2.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface DateUtil : NSObject

// get hour minute eg:12:34
+ (NSString *)getHourMinuteFromTimestamp:(NSString *)timestamp;
// get month day eg:03/20
+ (NSString *)getMonthDayFromTimestamp:(NSString *)timestamp;
// get year eg:2021
+ (NSString *)getYearFromTimestamp:(NSString *)timestamp;

// get time eg:02-22 09:30
+ (NSString *)getTimeFromTimestamp:(NSString *)timestamp;

// get time eg:09:30
+ (NSString *)getHourTimeFromTimestamp:(NSString *)timestamp;

// check if same minute
+ (BOOL)isSameMinuteInterval:(NSString *)fromTimestamp toTimestamp:(NSString *)toTimestamp interval:(NSInteger)interval;

// check if same hour
+ (BOOL)isSameHourInterval:(NSString *)fromTimestamp toTimestamp:(NSString *)toTimestamp interval:(NSInteger)interval;

// check if same day
+ (BOOL)isSameDay:(NSString *)fromTimestamp toTimestamp:(NSString *)toTimestamp;

// check if same week
+ (BOOL)isSameWeek:(NSString *)fromTimestamp toTimestamp:(NSString *)toTimestamp;

// check if same month
+ (BOOL)isSameMonth:(NSString *)fromTimestamp toTimestamp:(NSString *)toTimestamp;

// check if same year
+ (BOOL)isSameYear:(NSString *)fromTimestamp toTimestamp:(NSString *)toTimestamp;

@end

NS_ASSUME_NONNULL_END
