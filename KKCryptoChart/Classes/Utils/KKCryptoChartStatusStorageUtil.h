//
//  KKStatusStorageUtil.h
//  KKCryptoChart
//
//  Created by Henry on 2021/11/8.
//

#import <Foundation/Foundation.h>
@class TabbarSelectUtil;

@interface KKCryptoChartStatusStorageUtil : NSObject

+ (void)removeAllTabbarStatus;

@end
