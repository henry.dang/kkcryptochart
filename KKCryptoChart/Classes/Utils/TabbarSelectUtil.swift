//
//  TabbarSelectUtil.swift
//  LineDemo
//
//  Created by slowdony on 2021/10/27.
//

import Foundation

@objc open class TabbarSelectUtil:NSObject {
    
    @objc public static let share = TabbarSelectUtil()
    private override init() {}
    
    let KChartTabbarBtnSelectType = "KChartTabbarBtnSelectType"
    
    let KChartTabbarMoreBtnSelectType = "kChartTabbarMoreBtnSelectType"
    
    let KChartTabbarIndicatorMainBtnSelectorType = "kChartTabbarIndicatorMainBtnSelectorType"
    let KChartTabbarIndicatorSubBtnSelectorType = "kChartTabbarIndicatorSubBtnSelectorType"
    let KChartTabbarIndicatorMainMASelectorType = "KChartTabbarIndicatorMainMASelectorType"
    
    
    @objc open func removeAllTabbarStatus() {
        removeTabbarSelectTag()
        removeTabbarSelectMoreBtnTag()
        removeTabbarIndicatorMainBtnTag()
        removeTabbarIndicatorSubBtnTag()
    }
    
    // tabbar select
    @objc open func setTabbarSelectBtnTag(tag:Int){
        
        self.setTabbarSelect(tag: tag, key: KChartTabbarBtnSelectType)
    }
    
    @objc open func getTabbarSelectBtnTag() -> Int {
        return  self.getTabbarSelect(key: KChartTabbarBtnSelectType)
    }
    
    @objc open func removeTabbarSelectTag() {
        self.removeTabbarSelect(key: KChartTabbarBtnSelectType)
    }
    
    // more
    @objc open func setTabbarSelectMoreBtnTag(tag:Int){
        
        self.setTabbarSelect(tag: tag, key: KChartTabbarMoreBtnSelectType)
    }
    
    @objc open func getTabbarSelectMoreBtnTag() -> Int {
        return  self.getTabbarSelect(key: KChartTabbarMoreBtnSelectType)
    }
    
    @objc open func removeTabbarSelectMoreBtnTag(){
        self.removeTabbarSelect(key: KChartTabbarMoreBtnSelectType)
    }
    
    // main
    @objc open func setTabbarIndicatorMainBtnTag(tag:Int){
        
        self.setTabbarSelect(tag: tag, key: KChartTabbarIndicatorMainBtnSelectorType)
    }
    
    @objc open func getTabbarIndicatorMainBtnTag() -> Int {
        return  self.getTabbarSelect(key: KChartTabbarIndicatorMainBtnSelectorType)
    }
    
    @objc open func removeTabbarIndicatorMainBtnTag(){
        self.removeTabbarSelect(key: KChartTabbarIndicatorMainBtnSelectorType)
    }
    
    //MA MA默认选中，记录是否默认选中
    @objc open func setTabbarIndicatorMainMASelect(status:Int) {
        self.setTabbarSelect(tag: status, key: KChartTabbarIndicatorMainMASelectorType)
    }
    
    @objc open func getTabbarIndicatorMainMASelect() -> Int {
        return  self.getTabbarSelect(key: KChartTabbarIndicatorMainMASelectorType)
    }
    
    // sub
    @objc open func setTabbarIndicatorSubBtnTag(tag:Array<Any>){
        self.setTabbarArraySelect(tagArray: tag, key: KChartTabbarIndicatorSubBtnSelectorType);
    }
    
    @objc open func getTabbarIndicatorSubBtnTag() -> Array<Any> {
        return self.getTabbarArraySelect(key: KChartTabbarIndicatorSubBtnSelectorType);
    }
    
    @objc open func removeTabbarIndicatorSubBtnTag(){
        self.removeTabbarArraySelect(key: KChartTabbarIndicatorSubBtnSelectorType)
    }
    
    private func setTabbarSelect(tag:Int,key:String){
        UserDefaults.standard.set(tag, forKey:key)
        UserDefaults.standard.synchronize()
    }
    
    private func getTabbarSelect(key:String) -> Int{
        let tag = UserDefaults.standard.integer(forKey: key)
        return tag
    }
    
    private func removeTabbarSelect(key:String) {
        UserDefaults.standard.removeObject(forKey: key)
        UserDefaults.standard.synchronize()
    }
    
    private func setTabbarArraySelect(tagArray:Array<Any>,key:String){
        UserDefaults.standard.set(tagArray, forKey:key)
        UserDefaults.standard.synchronize()
    }
    
    private func getTabbarArraySelect(key:String) -> Array<Any>{
        guard let tagArray = UserDefaults.standard.array(forKey: key) else { return [] }
        return tagArray
    }
    
    private func removeTabbarArraySelect(key:String) {
        UserDefaults.standard.removeObject(forKey: key)
        UserDefaults.standard.synchronize()
    }
}
