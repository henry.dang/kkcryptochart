//
//  DateUtil.m
//  KikiChartsDemo
//
//  Created by apple on 2021/3/2.
//

#import "DateUtil.h"

@implementation DateUtil

+ (NSString *)getYearToMinuteFromTimestamp:(NSString *)timestamp {
    if (timestamp == nil) {
        return nil;
    }
    return [self getFormatteredString:@"yyyy MM-dd HH:mm" timestamp:timestamp];
}

+ (NSString *)getYearToHourFromTimestamp:(NSString *)timestamp {
    if (timestamp == nil) {
        return nil;
    }
    return [self getFormatteredString:@"yyyy MM-dd HH" timestamp:timestamp];
}

+ (NSString *)getHourMinuteFromTimestamp:(NSString *)timestamp {
    if (timestamp == nil) {
        return nil;
    }
    NSString *time = [self getFormatteredString:@"MM-dd HH:mm" timestamp:timestamp];
    if ([time isEqualToString:@"00:00"]) {
        return [self getMonthDayFromTimestamp:timestamp];
    }
    return time;
}

+ (NSString *)getYearMonthDayFromTimestamp:(NSString *)timestamp {
    if (timestamp == nil) {
        return nil;
    }
    return [self getFormatteredString:@"yyyy MM/dd" timestamp:timestamp];
}

+ (NSString *)getMonthDayFromTimestamp:(NSString *)timestamp {
    if (timestamp == nil) {
        return nil;
    }
    return [self getFormatteredString:@"yyyy-MM-dd" timestamp:timestamp];
}

+ (NSString *)getYearFromTimestamp:(NSString *)timestamp {
    if (timestamp == nil) {
        return nil;
    }
    return [self getFormatteredString:@"yyyy" timestamp:timestamp];
}

+ (NSString *)getYearMonthFromTimestamp:(NSString *)timestamp {
    if (timestamp == nil) {
        return nil;
    }
    return [self getFormatteredString:@"yyyy MM" timestamp:timestamp];
}

+ (NSString *)getTimeFromTimestamp:(NSString *)timestamp{
    if (timestamp == nil) {
        return nil;
    }
    return [self getFormatteredString:@"MM-dd HH:mm" timestamp:timestamp];
}

+ (NSString *)getHourTimeFromTimestamp:(NSString *)timestamp{
    if (timestamp == nil) {
        return nil;
    }
    return [self getFormatteredString:@"HH:mm" timestamp:timestamp];
}

+ (NSString *)getFormatteredString:(NSString *)formatterString timestamp:(NSString *)timestamp {
    long long time = timestamp.longLongValue;
    if (timestamp.length == 13) {
        time /= 1000;
    }
    NSDate *date = [[NSDate alloc] initWithTimeIntervalSince1970:time];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:formatterString];

    return [formatter stringFromDate:date];
}

+ (BOOL)isSameMinuteInterval:(NSString *)fromTimestamp toTimestamp:(NSString *)toTimestamp interval:(NSInteger)interval {
    if (fromTimestamp == nil || toTimestamp == nil) {
        return NO;
    }
    if (fromTimestamp.length != toTimestamp.length) {
        return NO;
    }
    
    if (interval <= 0) {
        return NO;
    }
    NSDate *fromDate = [[NSDate alloc] initWithTimeIntervalSince1970:fromTimestamp.longLongValue];
    NSDate *toDate= [[NSDate alloc] initWithTimeIntervalSince1970:toTimestamp.longLongValue];
    
    NSTimeInterval timeInterval = [fromDate timeIntervalSinceDate:toDate];
    
    if (timeInterval < 0) {
        return NO;
    }
    
    return timeInterval < (60 * interval);
}

+ (BOOL)isSameHourInterval:(NSString *)fromTimestamp toTimestamp:(NSString *)toTimestamp interval:(NSInteger)interval {
    if (interval <= 0) {
        return NO;
    }
    NSDate *fromDate = [[NSDate alloc] initWithTimeIntervalSince1970:fromTimestamp.longLongValue];
    NSDate *toDate= [[NSDate alloc] initWithTimeIntervalSince1970:toTimestamp.longLongValue];
    
    NSTimeInterval timeInterval = [fromDate timeIntervalSinceDate:toDate];
    
    if (timeInterval < 0) {
        return NO;
    }
    
    return timeInterval < (60 * 60 * interval);
}

+ (BOOL)isSameDay:(NSString *)fromTimestamp toTimestamp:(NSString *)toTimestamp {
    if (fromTimestamp == nil || toTimestamp == nil) {
        return NO;
    }
    if (fromTimestamp.length != toTimestamp.length) {
        return NO;
    }

    return [[self getYearMonthDayFromTimestamp:fromTimestamp] isEqualToString:[self getYearMonthDayFromTimestamp:toTimestamp]];
}

+ (BOOL)isSameWeek:(NSString *)fromTimestamp toTimestamp:(NSString *)toTimestamp {
    long long fromTime = fromTimestamp.longLongValue;
    if (fromTimestamp.length == 13) {
        fromTime /= 1000;
    }
    NSDate *fromDate = [[NSDate alloc] initWithTimeIntervalSince1970:fromTime];
    
    long long toTime = toTimestamp.longLongValue;
    if (toTimestamp.length == 13) {
        toTime /= 1000;
    }
    NSDate *toDate = [[NSDate alloc] initWithTimeIntervalSince1970:toTime];

    // 日历对象
    NSCalendar *calendar = [NSCalendar currentCalendar];
    unsigned unitFlag = NSCalendarUnitWeekOfYear | NSCalendarUnitYearForWeekOfYear;
    NSDateComponents *comp1 = [calendar components:unitFlag fromDate:fromDate];
    NSDateComponents *comp2 = [calendar components:unitFlag fromDate:toDate];
    /// 年份和周数相同，即判断为同一周
    /// NSCalendarUnitYearForWeekOfYear已经帮转换不同年份的周所属了，比如2019.12.31是等于2020的。这里不使用year，使用用yearForWeekOfYear
    return (([comp1 yearForWeekOfYear] == [comp2 yearForWeekOfYear]) && ([comp1 weekOfYear] == [comp2 weekOfYear]));
}

+ (BOOL)isSameMonth:(NSString *)fromTimestamp toTimestamp:(NSString *)toTimestamp {
    if (fromTimestamp == nil || toTimestamp == nil) {
        return NO;
    }
    if (fromTimestamp.length != toTimestamp.length) {
        return NO;
    }

    return [[self getYearMonthFromTimestamp:fromTimestamp] isEqualToString:[self getYearMonthFromTimestamp:toTimestamp]];
}

+ (BOOL)isSameYear:(NSString *)fromTimestamp toTimestamp:(NSString *)toTimestamp {
    if (fromTimestamp == nil || toTimestamp == nil) {
        return NO;
    }
    if (fromTimestamp.length != toTimestamp.length) {
        return NO;
    }

    return [[self getYearFromTimestamp:fromTimestamp] isEqualToString:[self getYearFromTimestamp:toTimestamp]];
}

@end
