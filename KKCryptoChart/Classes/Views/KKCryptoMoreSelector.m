//
//  KKCryptoMoreSelector.m
//  KikiChartsDemo
//
//  Created by apple on 2021/3/10.
//

#import "KKCryptoMoreSelector.h"
#import "KKCryptoChartConstant.h"
#import "BundleUtil.h"
#import "KKCryptoChart-Swift.h"

@interface KKCryptoMoreSelector()
@property (strong, nonatomic) NSArray<UIButton *> *btns;
@property (weak, nonatomic) IBOutlet UIView *buttonsContainerView;
@property (weak, nonatomic) IBOutlet UIButton *firstBtn;
@property (weak, nonatomic) IBOutlet UIButton *secondBtn;
@property (weak, nonatomic) IBOutlet UIButton *thirdBtn;
@property (weak, nonatomic) IBOutlet UIButton *fourthBtn;
@property (weak, nonatomic) IBOutlet UIButton *fifthBtn;
@property (weak, nonatomic) IBOutlet UIButton *sixthBtn;
@end


@implementation KKCryptoMoreSelector

- (instancetype)init {
    NSBundle *bundle = [BundleUtil getBundle];
    self = [[bundle loadNibNamed:@"KKCryptoMoreSelector" owner:nil options:nil] lastObject];
    if (self) {
        [self setUpBorder];
    }
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    self.btns = @[self.firstBtn, self.secondBtn, self.thirdBtn, self.fourthBtn, self.fifthBtn, self.sixthBtn];
}

- (void)setUpBorder {
    self.layer.cornerRadius = 12;
    self.layer.shadowColor = [UIColor blackColor].CGColor;
    self.layer.shadowOffset = CGSizeMake(0, 0);
    self.layer.shadowOpacity = 0.1f;
    self.layer.shadowRadius = 4;
}

- (void)setUpTypes:(NSArray<NSNumber *> *)types titles:(NSArray<NSString *> *)titles {
    if (!types || !titles || types.count != MORE_SELECTOR_COUNT || titles.count != MORE_SELECTOR_COUNT) {
        return;
    }
    for (NSInteger i = 0; i < types.count; i++) {
        self.btns[i].tag = types[i].integerValue;
        [self.btns[i] setTitle:titles[i] forState:UIControlStateNormal];
        NSInteger selectType = [[TabbarSelectUtil share]getTabbarSelectMoreBtnTag];
        if (selectType == types[i].integerValue){
            [self.btns[i] setSelected:YES];
            [self.btns[i] setBackgroundColor:INDICATOR_BUTTON_SELECTED_BACKGROUND_COLOR];
        }
    }
}

#pragma mark - Actions

- (IBAction)firstBtnSelected:(id)sender {
    [self btnSelected:(UIButton *)sender];
}

- (IBAction)secondBtnSelected:(id)sender {
    [self btnSelected:(UIButton *)sender];
}

- (IBAction)thirdBtnSelected:(id)sender {
    [self btnSelected:(UIButton *)sender];
}

- (IBAction)fourthBtnSelected:(id)sender {
    [self btnSelected:(UIButton *)sender];
}

- (IBAction)fifthBtnSelected:(id)sender {
    [self btnSelected:(UIButton *)sender];
}

- (IBAction)sixthBtnSelected:(id)sender {
    [self btnSelected:(UIButton *)sender];
}

- (void)btnSelected:(UIButton *)btn {
    [[TabbarSelectUtil share]setTabbarSelectMoreBtnTagWithTag:btn.tag];
    BOOL isSelected = btn.selected;
    [self setAllBtnUnselected];
    [btn setSelected:!isSelected];
    [btn setBackgroundColor:btn.selected ? INDICATOR_BUTTON_SELECTED_BACKGROUND_COLOR : INDICATOR_BUTTON_DEFAULT_BACKGROUND_COLOR];
    if ([self.delegate respondsToSelector:@selector(didMoreSelectorSelected:title:)]) {
        [self.delegate didMoreSelectorSelected:btn.tag title:btn.currentTitle];
    }
}

- (void)setAllBtnUnselected {
    [self.firstBtn setSelected:NO];
    [self.firstBtn setBackgroundColor:INDICATOR_BUTTON_DEFAULT_BACKGROUND_COLOR];
    [self.secondBtn setSelected:NO];
    [self.secondBtn setBackgroundColor:INDICATOR_BUTTON_DEFAULT_BACKGROUND_COLOR];
    [self.thirdBtn setSelected:NO];
    [self.thirdBtn setBackgroundColor:INDICATOR_BUTTON_DEFAULT_BACKGROUND_COLOR];
    [self.fourthBtn setSelected:NO];
    [self.fourthBtn setBackgroundColor:INDICATOR_BUTTON_DEFAULT_BACKGROUND_COLOR];
    [self.fifthBtn setSelected:NO];
    [self.fifthBtn setBackgroundColor:INDICATOR_BUTTON_DEFAULT_BACKGROUND_COLOR];
    [self.sixthBtn setSelected:NO];
    [self.sixthBtn setBackgroundColor:INDICATOR_BUTTON_DEFAULT_BACKGROUND_COLOR];
}

@end
