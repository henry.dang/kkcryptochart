//
//  KKCryptoChartTabBar.m
//  KikiChartsDemo
//
//  Created by apple on 2021/3/9.
//

#define TAB_BAR_HORIZONTAL_MARGIN 5
#define TAB_BAR_BUTTON_WIDTH 36
#define TAB_BAR_BUTTON_HEIGHT 14
#define TAB_BAR_BUTTON_NORMAL_TITLE_COLOR [UIColor colorWithRed:67 / 255.f green:75 / 255.f blue:95 / 255.f alpha:1.0f]
#define TAB_BAR_BACKGROUND_COLOR [UIColor colorWithRed:246 / 255.f green:245 / 255.f blue:250 / 255.f alpha:1.0f]
#define TAB_BAR_BACKGROUND_SELECTED_COLOR [UIColor colorWithRed:111 / 255.f green:64 / 255.f blue:238 / 255.f alpha:1.0f]


#import "KKCryptoChartTabBar.h"
#import "KKCryptoChartConstant.h"
#import "KKCryptoChart-Swift.h"
@interface KKCryptoChartTabBar()

@property (strong, nonatomic) NSMutableArray<UIButton *> *tabItems;
@property (strong, nonatomic) UIView *bottomLine;
@property (strong, nonatomic) UIButton *currentButton;
@property (strong, nonatomic) NSString *moreOriginTitle;
@property (strong, nonatomic) NSString *indicatorOriginTitle;
@property (strong, nonatomic) NSString *suffixString;
@property (assign, nonatomic) BOOL isChoosedMoreTime;
@property (assign, nonatomic) BOOL isSetup;

@end

@implementation KKCryptoChartTabBar

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        _tabItems = [[NSMutableArray alloc] init];
        _isChoosedMoreTime = NO;
        _isSetup = NO;
        [self initUI:frame];
    }
    return self;
}

- (void)initUI:(CGRect)frame {
    self.backgroundColor = TAB_BAR_BACKGROUND_COLOR;
    self.layer.cornerRadius = 3.0f;
    [self initTabBarUI:frame];
}

- (void)initTabBarUI:(CGRect)frame {
    for (NSInteger i = 0; i < TAB_BAR_COUNT; i++) {
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        [btn setTitleColor:TAB_BAR_BUTTON_NORMAL_TITLE_COLOR forState:UIControlStateNormal];
        //初始化,如果没值默认为0按钮选中
        NSInteger index =  [[TabbarSelectUtil share] getTabbarSelectBtnTag];
        if (index == 0 && i == 0) {
            self.currentButton = btn;
            [btn setSelected:YES];
        }
        btn.titleLabel.font = [UIFont systemFontOfSize:11.f];
        if (i < TAB_BAR_COUNT - 2) {
            [btn setBackgroundColor:btn.selected ? TAB_BAR_BACKGROUND_SELECTED_COLOR : TAB_BAR_BACKGROUND_COLOR];
            btn.layer.cornerRadius = 8.0f;
            [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
        } else {
            [btn setBackgroundColor:TAB_BAR_BACKGROUND_COLOR];
        }
        btn.titleLabel.minimumScaleFactor = 0.8f;
        btn.titleLabel.adjustsFontSizeToFitWidth = YES;
        [btn addTarget:self action:@selector(tabBarSelected:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:btn];
        [self.tabItems addObject:btn];
    }
}

- (void)layoutSubviews {
    KKLog(@"KKCryptoChartTabBar layoutSubviews");
    CGFloat buttonPadding = (self.frame.size.width - 2 * TAB_BAR_HORIZONTAL_MARGIN - TAB_BAR_COUNT * TAB_BAR_BUTTON_WIDTH) / (TAB_BAR_COUNT - 1);
    CGFloat buttonOriginY = (self.frame.size.height - TAB_BAR_BUTTON_HEIGHT) / 2;
    for (NSInteger i = 0; i < self.tabItems.count; i++) {
        [self.tabItems[i] setFrame:CGRectMake(TAB_BAR_HORIZONTAL_MARGIN + i * (TAB_BAR_BUTTON_WIDTH + buttonPadding), buttonOriginY, TAB_BAR_BUTTON_WIDTH, TAB_BAR_BUTTON_HEIGHT)];
    }
}

- (void)setUpTypes:(NSArray<NSNumber *> *)types titles:(NSArray<NSString *> *)titles {
    if (!types || !titles) {
        return;
    }
    if (types.count != TAB_BAR_COUNT || titles.count != TAB_BAR_COUNT) {
        return;
    }
    if (self.isSetup) {
        return;
    }
    KKLog(@"KKCryptoChartTabBar setUpTypes");
    for (NSInteger i = 0; i < types.count; i++) {
        self.tabItems[i].tag = types[i].integerValue;
        self.tabItems[i].accessibilityIdentifier = [NSString stringWithFormat:@"com.kikitrade.KKCryptoChart_%ld", (long)i];
        KKLog(@"KKCryptoChartTabBar setUpTypes accessibilityIdentifier:%@", self.tabItems[i].accessibilityIdentifier);
        if (i == TAB_BAR_COUNT - 1) {
            self.indicatorOriginTitle = titles[i];
            [self.tabItems[i] setAttributedTitle:[self setupButtonNormalTitle:titles[i]] forState:UIControlStateNormal];
            [self.tabItems[i] setAttributedTitle:[self setupButtonSelectedTitle:titles[i]] forState:UIControlStateSelected];
        } else if (i == TAB_BAR_COUNT - 2) {
            self.moreOriginTitle = titles[i];
            [self.tabItems[i] setAttributedTitle:[self setupButtonNormalTitle:titles[i]] forState:UIControlStateNormal];
            [self.tabItems[i] setAttributedTitle:[self setupButtonSelectedTitle:titles[i]] forState:UIControlStateSelected];
        } else {
            [self.tabItems[i] setTitle:titles[i] forState:UIControlStateNormal];
            
            //设置tabbar前四个按钮的选中状态
            NSInteger selectType =  [[TabbarSelectUtil share] getTabbarSelectBtnTag];
            if (selectType == types[i].integerValue){
                self.tabItems[i].selected = YES;
                [self.tabItems[i] setBackgroundColor:TAB_BAR_BACKGROUND_SELECTED_COLOR];
                self.currentButton = self.tabItems[i];
            }
        }
    }
    self.isSetup = YES;
}

- (void)setupSuffixString:(NSString *)suffixString {
    self.suffixString = suffixString;
}

// 拼接◢
- (NSMutableAttributedString *)setupTriangleButtonTitle:(NSString *) title firstColor:(UIColor *)firstColor secondColor:(UIColor *)secondColor {
    if (!title || title.length < 2) {
        return nil;
    }
    NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc] initWithString:title];
    [attrString addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:11] range:NSMakeRange(0, title.length - 1)];
    [attrString addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:5] range:NSMakeRange(title.length - 1, 1)];
    [attrString addAttribute:NSForegroundColorAttributeName value:firstColor range:NSMakeRange(0, title.length - 1)];
    [attrString addAttribute:NSForegroundColorAttributeName value:secondColor range:NSMakeRange(title.length - 1, 1)];
    
    return attrString;
}

- (NSMutableAttributedString *)setupButtonNormalTitle:(NSString *)title {
    if (!title || title.length < 2) {
        return nil;
    }
    return [self setupTriangleButtonTitle:title firstColor:TAB_BAR_BUTTON_NORMAL_TITLE_COLOR secondColor:TAB_BAR_BUTTON_NORMAL_TITLE_COLOR];
}

- (NSMutableAttributedString *)setupButtonSelectedTitle:(NSString *)title {
    if (!title || title.length < 2) {
        return nil;
    }
    return [self setupTriangleButtonTitle:title firstColor:TAB_BAR_BUTTON_NORMAL_TITLE_COLOR secondColor:TAB_BAR_BACKGROUND_SELECTED_COLOR];
}

- (void)tabBarSelected:(UIButton *)button {
    
    if (button == self.tabItems[TAB_BAR_COUNT - 2]) {
        [self.tabItems[TAB_BAR_COUNT - 1] setSelected:NO];
        self.currentButton = button;
        if (!self.isChoosedMoreTime) {
            [button setSelected:!button.isSelected];
            KKLog(@"tabBarSelected more button isSelected:%lu", (unsigned long)button.isSelected);
            [button setBackgroundColor:TAB_BAR_BACKGROUND_COLOR];
        }
        self.currentButton = button;
    } else if (button == self.tabItems[TAB_BAR_COUNT - 1]) {
        if (!self.isChoosedMoreTime) {
            [self.tabItems[TAB_BAR_COUNT - 2] setSelected:NO];
        }
        self.currentButton = button;
        [button setSelected:!button.isSelected];
        KKLog(@"tabBarSelected ind button isSelected:%lu", (unsigned long)button.isSelected);
        [button setBackgroundColor:TAB_BAR_BACKGROUND_COLOR];
    } else {
        KKLog(@"tabBarSelected else");
       
       
        if (self.currentButton != button) {
            //记录tabbar前4个按钮的选中tag
            [[TabbarSelectUtil share]setTabbarSelectBtnTagWithTag:button.tag];
            //当选中tabbar 前四个任意一个时,需要移除moreview的记录
            [[TabbarSelectUtil share]removeTabbarSelectMoreBtnTag];
            
            [self setTimeTabBarButtonUnselected];
            [button setSelected:YES];
            [button setBackgroundColor:button.selected ? TAB_BAR_BACKGROUND_SELECTED_COLOR : TAB_BAR_BACKGROUND_COLOR];
            self.currentButton = button;
            [self resetMoreBtn];
        }
    }
    if ([self.delegate respondsToSelector:@selector(didTabItemSelected:title:)]) {
        [self.delegate didTabItemSelected:button.tag title:button.currentTitle];
    }
}

- (void)setTimeTabBarButtonUnselected {
    for (NSInteger i = 0; i < self.tabItems.count; i++) {
        if (i < TAB_BAR_COUNT - 2) {
            [self.tabItems[i] setSelected:NO];
            [self.tabItems[i] setBackgroundColor:TAB_BAR_BACKGROUND_COLOR];
        }
    }
}

- (void)updateMoreTabItem:(NSInteger)type title:(NSString *)title {
    for (UIButton *btn in self.tabItems) {
        if (btn.tag == type) {
            KKLog(@"KKCryptoChartTabBar updateMoreTabItem title:%@", title);
            [btn setSelected:YES];
            KKLog(@"KKCryptoChartTabBar updateMoreTabItem btn state:%lu", (unsigned long)btn.state);
            [self setTimeTabBarButtonUnselected];
            [btn setBackgroundColor:btn.selected ? TAB_BAR_BACKGROUND_SELECTED_COLOR : TAB_BAR_BACKGROUND_COLOR];

            NSString *compoundTitle = [NSString stringWithFormat:@"%@%@", title, self.suffixString];
            [btn setAttributedTitle:[self setupTriangleButtonTitle:compoundTitle firstColor:[UIColor whiteColor] secondColor:[UIColor whiteColor]] forState:UIControlStateSelected];
            btn.layer.cornerRadius = 8.0f;
            self.isChoosedMoreTime = YES;
        }
    }
}

- (void)updateSelectorTabStatus {
    KKLog(@"KKCryptoChartTabBar updateSelectorTabStatus");
    if (!self.isChoosedMoreTime) {
        [self.tabItems[TAB_BAR_COUNT - 2] setSelected:NO];
    }
    [self.tabItems[TAB_BAR_COUNT - 1] setSelected:NO];
}

- (void)resetMoreBtn {
    KKLog(@"KKCryptoChartTabBar resetMoreBtn");
    [self.tabItems[TAB_BAR_COUNT - 2] setSelected:NO];
    self.tabItems[TAB_BAR_COUNT - 2].layer.cornerRadius = 0.0f;
    [self.tabItems[TAB_BAR_COUNT - 2] setBackgroundColor:TAB_BAR_BACKGROUND_COLOR];
    [self.tabItems[TAB_BAR_COUNT - 2] setTitleColor:TAB_BAR_BUTTON_NORMAL_TITLE_COLOR forState:UIControlStateNormal];
    [self.tabItems[TAB_BAR_COUNT - 2] setAttributedTitle:[self setupButtonNormalTitle:self.moreOriginTitle] forState:UIControlStateNormal];
    [self.tabItems[TAB_BAR_COUNT - 2] setAttributedTitle:[self setupButtonSelectedTitle:self.moreOriginTitle] forState:UIControlStateSelected];
    self.isChoosedMoreTime = NO;
}

@end
