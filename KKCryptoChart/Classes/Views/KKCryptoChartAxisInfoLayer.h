//
//  KKCryptoChartAxisInfoLayer.h
//  KKCryptoChart
//
//  Created by Henry on 2022/3/7.
//

#import <QuartzCore/QuartzCore.h>

@interface KKCryptoChartAxisInfoLayer : CALayer

@property (nonatomic, copy) NSString *text;
@property (nonatomic, assign) NSInteger fontSize;
@property (nonatomic, assign) CATextLayerAlignmentMode alignmentMode;
@property (nonatomic, assign) CGFloat xOffset;
@property (nonatomic, assign) CGFloat yOffset;

@end
