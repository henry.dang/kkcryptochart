//
//  KKCryptoChart.h
//  KikiChartsDemo
//
//  Created by apple on 2021/3/1.
//

#import <UIKit/UIKit.h>
@import KKCharts;

@class KKCryptoChartView,KKCryptoChartGlobalConfig;

@protocol KKCryptoChartBugReportDelegate <NSObject>

- (void)didErrorOccurred:(KKCryptoChartView *)chart error:(NSString *)error;

@end

/**
 multi-chart view, include two or three charts
 */
@interface KKCryptoChartView : UIView
/**
 init config
 */
@property (nonatomic, strong) KKCryptoChartGlobalConfig *config;
@property (nonatomic, assign) id<KKCryptoChartBugReportDelegate> bugReportDelegate;

@end

