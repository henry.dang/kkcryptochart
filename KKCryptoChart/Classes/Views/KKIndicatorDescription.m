//
//  KKIndicatorDescription.m
//  KKCryptoChart
//
//  Created by apple on 2021/7/19.
//

#import "KKIndicatorDescription.h"
#import "KKCryptoChartConstant.h"
#import "KKCryptoChartDataManager.h"
#import "KKCryptoChartModel.h"

@interface KKIndicatorDescription ()

@property (nonatomic, strong) UILabel *indicatorLabel;

@end

@implementation KKIndicatorDescription

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self initUI:frame];
    }
    return self;
}

- (void)initUI:(CGRect)frame {
    self.indicatorLabel = [[UILabel alloc] init];
    self.indicatorLabel.font = [UIFont systemFontOfSize:8.0f];
    self.indicatorLabel.numberOfLines = 1;
    self.indicatorLabel.adjustsFontSizeToFitWidth = YES;
    [self addSubview:self.indicatorLabel];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    [self.indicatorLabel setFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
//    KKLog(@"KKIndicatorDescription layoutSubviews x=%f, y=%f, width=%f, height=%f", self.frame.origin.x, self.frame.origin.y, self.frame.size.width, self.frame.size.height);
}

- (void)setupDescription:(KKCryptoChartModel *)model type:(KKIndicator)type config:(KKCryptoChartGlobalConfig *)config {
    if (type == KKIndicatorNone && !model) {
        self.indicatorLabel.attributedText = nil;
        [self setNeedsLayout];
        return;
    }
    NSAttributedString *attrString = [self setupAttributeString:[self generateDescriptionText:model type:type config:config]];
//    KKLog(@"KKIndicatorDescription setupDescription attributedText:%@", attrString);
    self.indicatorLabel.attributedText = attrString;
    [self setNeedsLayout];
}

- (NSString *)generateDescriptionText:(KKCryptoChartModel *)model type:(KKIndicator)type config:(KKCryptoChartGlobalConfig *)config {
    NSArray<NSString *> *cycles = [[KKCryptoChartDataManager shareManager] getIndicatorTimeCycles:type];
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    [formatter setMaximumFractionDigits:config.coinPrecision.integerValue];
    [formatter setMinimumFractionDigits:config.coinPrecision.integerValue];
    NSMutableString *mutableText = [NSMutableString string];
    switch (type) {
        case KKIndicatorMA: {
            if (cycles.count == 3) {
                [mutableText appendString:[NSString stringWithFormat:@"MA(%@):%@  ", cycles[0], [formatter stringFromNumber:model.MA.MA1]]];
                [mutableText appendString:[NSString stringWithFormat:@"MA(%@):%@  ", cycles[1], [formatter stringFromNumber:model.MA.MA2]]];
                [mutableText appendString:[NSString stringWithFormat:@"MA(%@):%@", cycles[2], [formatter stringFromNumber:model.MA.MA3]]];
            }
        }
            break;
        case KKIndicatorEMA: {
            if (cycles.count == 3) {
                [mutableText appendString:[NSString stringWithFormat:@"EMA(%@):%@  ", cycles[0], [formatter stringFromNumber:model.EMA.EMA1]]];
                [mutableText appendString:[NSString stringWithFormat:@"EMA(%@):%@  ", cycles[1], [formatter stringFromNumber:model.EMA.EMA2]]];
                [mutableText appendString:[NSString stringWithFormat:@"EMA(%@):%@", cycles[2], [formatter stringFromNumber:model.EMA.EMA3]]];
            }
        }
            break;
        case KKIndicatorBOLL: {
            if (cycles.count == 2) {
                [mutableText appendString:[NSString stringWithFormat:@"BOLL(%@,%@)  ", cycles[0], cycles[1]]];
                [mutableText appendString:[NSString stringWithFormat:@"UP:%@  ", [formatter stringFromNumber:model.BOLL.UP]]];
                [mutableText appendString:[NSString stringWithFormat:@"MB:%@  ", [formatter stringFromNumber:model.BOLL.MID]]];
                [mutableText appendString:[NSString stringWithFormat:@"DN:%@", [formatter stringFromNumber:model.BOLL.LOW]]];
            }
        }
            break;
        case KKIndicatorTD: {
            if (model.TD.buySetupIndex.intValue > 0) {
                [mutableText appendString:[NSString stringWithFormat:@"TDDown:%@", model.TD.buySetupIndex]];
            } else {
                [mutableText appendString:[NSString stringWithFormat:@"TDUp:%@", model.TD.sellSetupIndex.intValue > 0 ? model.TD.sellSetupIndex : @"0"]];
            }
        }
            break;
        case KKIndicatorMACD: {
            if (cycles.count == 3) {
                [mutableText appendString:[NSString stringWithFormat:@"MACD(%@,%@,%@)  ", cycles[0], cycles[1], cycles[2]]];
                [mutableText appendString:[NSString stringWithFormat:@"DIFF:%@  ", [formatter stringFromNumber:model.MACD.DIFF]]];
                [mutableText appendString:[NSString stringWithFormat:@"DEA:%@  ", [formatter stringFromNumber:model.MACD.DEA]]];
                [mutableText appendString:[NSString stringWithFormat:@"MACD:%@", [formatter stringFromNumber:model.MACD.MACD]]];
            }
        }
            break;
        case KKIndicatorKDJ:{
            if (cycles.count == 3) {
                [formatter setMaximumFractionDigits:2];
                [formatter setMinimumFractionDigits:2];
                [mutableText appendString:[NSString stringWithFormat:@"KDJ(%@,%@,%@)  ", cycles[0], cycles[1], cycles[2]]];
                [mutableText appendString:[NSString stringWithFormat:@"K:%@  ", [formatter stringFromNumber:model.KDJ.K]]];
                [mutableText appendString:[NSString stringWithFormat:@"D:%@  ", [formatter stringFromNumber:model.KDJ.D]]];
                [mutableText appendString:[NSString stringWithFormat:@"J:%@", [formatter stringFromNumber:model.KDJ.J]]];
            }
        }
            break;
        case KKIndicatorRSI: {
            if (cycles.count == 3) {
                [formatter setMaximumFractionDigits:2];
                [formatter setMinimumFractionDigits:2];
                [mutableText appendString:[NSString stringWithFormat:@"RSI(%@):%@  ", cycles[0], [formatter stringFromNumber:model.RSI.RSI1]]];
                [mutableText appendString:[NSString stringWithFormat:@"RSI(%@):%@  ", cycles[1], [formatter stringFromNumber:model.RSI.RSI2]]];
                [mutableText appendString:[NSString stringWithFormat:@"RSI(%@):%@", cycles[2], [formatter stringFromNumber:model.RSI.RSI3]]];
            }
        }
            break;
        case KKIndicatorWR: {
            if (cycles.count == 2) {
                [formatter setMaximumFractionDigits:2];
                [formatter setMinimumFractionDigits:2];
                [mutableText appendString:[NSString stringWithFormat:@"WR(%@):%@  ", cycles[0], [formatter stringFromNumber:model.WR.WR1]]];
                [mutableText appendString:[NSString stringWithFormat:@"WR(%@):%@", cycles[1], [formatter stringFromNumber:model.WR.WR2]]];
            }
        }
            break;
        case KKIndicatorVolume:{
            NSNumberFormatter *volumeFormatter = [[NSNumberFormatter alloc] init];
            [volumeFormatter setMaximumFractionDigits:config.tradeVolumePrecision.integerValue];
            [volumeFormatter setMinimumFractionDigits:config.tradeVolumePrecision.integerValue];
            [mutableText appendString:[NSString stringWithFormat:@"VOL:%@", [volumeFormatter stringFromNumber:model.volume]]];
        }
            break;
        default:
            break;
    }
//    KKLog(@"KKIndicatorDescription generateDescriptionText text:%@", mutableText);
    return mutableText;
}

- (NSAttributedString *)setupAttributeString:(NSString *)string {
    if (string == nil || string.length == 0) {
        return nil;
    }
    NSString *spaceString = @"  ";
    NSArray<NSString *> *splitStrings = [string componentsSeparatedByString:spaceString];
//    KKLog(@"KKIndicatorDescription setupAttributeString splitStrings count:%lu", (unsigned long)splitStrings.count);
    if (splitStrings.count == 1) {
        NSAttributedString *attString = [[NSAttributedString alloc] initWithString:splitStrings[0] attributes:@{NSForegroundColorAttributeName : INDICATOR_DEFAULT_DESCRIPTION_COLOR}];
        return attString;
    } else if (splitStrings.count == 2) {
        NSAttributedString *attString0 = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@%@", splitStrings[0], spaceString] attributes:@{NSForegroundColorAttributeName : KK_CRYPTO_CHART_LINE_1_COLOR}];
        NSAttributedString *attString1 = [[NSAttributedString alloc] initWithString:splitStrings[1] attributes:@{NSForegroundColorAttributeName : KK_CRYPTO_CHART_LINE_2_COLOR}];
        NSMutableAttributedString * mutString = [[NSMutableAttributedString alloc] init];
        [mutString appendAttributedString:attString0];
        [mutString appendAttributedString:attString1];
        return mutString;
    } else if (splitStrings.count == 3) {
        KKLog(@"KKIndicatorDescription setupAttributeString splitStrings count is 3, splitStrings[0]:%@", splitStrings[0]);
        NSAttributedString *attString0 = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@%@", splitStrings[0], spaceString] attributes:@{NSForegroundColorAttributeName : KK_CRYPTO_CHART_LINE_1_COLOR}];
        NSAttributedString *attString1 = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@%@", splitStrings[1], spaceString] attributes:@{NSForegroundColorAttributeName : KK_CRYPTO_CHART_LINE_2_COLOR}];
        NSAttributedString *attString2 = [[NSAttributedString alloc] initWithString:splitStrings[2] attributes:@{NSForegroundColorAttributeName : KK_CRYPTO_CHART_LINE_3_COLOR}];
        NSMutableAttributedString * mutString = [[NSMutableAttributedString alloc] init];
        [mutString appendAttributedString:attString0];
        [mutString appendAttributedString:attString1];
        [mutString appendAttributedString:attString2];
        return mutString;
    } else if (splitStrings.count == 4) {
        NSAttributedString *attString0 = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@%@", splitStrings[0], spaceString] attributes:@{NSForegroundColorAttributeName : INDICATOR_DEFAULT_DESCRIPTION_COLOR}];
        NSAttributedString *attString1 = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@%@", splitStrings[1], spaceString] attributes:@{NSForegroundColorAttributeName : KK_CRYPTO_CHART_LINE_1_COLOR}];
        NSAttributedString *attString2 = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@%@", splitStrings[2], spaceString] attributes:@{NSForegroundColorAttributeName : KK_CRYPTO_CHART_LINE_2_COLOR}];
        NSAttributedString *attString3 = [[NSAttributedString alloc] initWithString:splitStrings[3] attributes:@{NSForegroundColorAttributeName : KK_CRYPTO_CHART_LINE_3_COLOR}];
        NSMutableAttributedString * mutString = [[NSMutableAttributedString alloc] init];
        [mutString appendAttributedString:attString0];
        [mutString appendAttributedString:attString1];
        [mutString appendAttributedString:attString2];
        [mutString appendAttributedString:attString3];
        return mutString;
    }
    return nil;
}

@end
