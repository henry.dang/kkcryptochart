//
//  KKIndicatorSelector.m
//  KikiChartsDemo
//
//  Created by apple on 2021/3/5.
//

#import "KKIndicatorSelector.h"
#import "KKCryptoChartConstant.h"
#import "BundleUtil.h"
#import "KKCryptoChartDataManager.h"
#import "KKCryptoChart-Swift.h"
@interface KKIndicatorSelector()
@property (weak, nonatomic) IBOutlet UILabel *mainLabel;
@property (weak, nonatomic) IBOutlet UILabel *subLabel;
@property (weak, nonatomic) IBOutlet UIButton *maBtn;
@property (weak, nonatomic) IBOutlet UIButton *emaBtn;
@property (weak, nonatomic) IBOutlet UIButton *bollBtn;
@property (weak, nonatomic) IBOutlet UIButton *tdBtn;
@property (weak, nonatomic) IBOutlet UIButton *mainHideBtn;

@property (weak, nonatomic) IBOutlet UIButton *macdBtn;
@property (weak, nonatomic) IBOutlet UIButton *kdjBtn;
@property (weak, nonatomic) IBOutlet UIButton *rsiBtn;
@property (weak, nonatomic) IBOutlet UIButton *wrBtn;
@property (weak, nonatomic) IBOutlet UIButton *sideHideBtn;
@property (nonatomic,strong) NSArray <UIButton *>*mainBtnArr;
@property (nonatomic,strong) NSArray <UIButton *>*subBtnArr;
@property (nonatomic,strong) NSMutableArray *selectArr;
@end

@implementation KKIndicatorSelector

- (instancetype)init {
    NSBundle *bundle = [BundleUtil getBundle];
    self = [[bundle loadNibNamed:@"KKIndicatorSelector" owner:nil options:nil] lastObject];
    KKLog(@"KKIndicatorSelector init %@", self.description);
    if (self) {
        [self setUpBorder];
        [self setUpSelector];
        [self setBtnArrs];
    }
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    KKLog(@"KKIndicatorSelector awakeFromNib");
}

- (void)setUpBorder {
    self.layer.cornerRadius = 12;
    self.layer.shadowColor = [UIColor blackColor].CGColor;
    self.layer.shadowOffset = CGSizeMake(0, 0);
    self.layer.shadowOpacity = 0.1f;
    self.layer.shadowRadius = 4;
}

- (void)setUpSelector {
    self.mainHideBtn.selected = YES;
    self.sideHideBtn.selected = YES;

    self.maBtn.tag = KKIndicatorMA;
    self.emaBtn.tag = KKIndicatorEMA;
    self.bollBtn.tag = KKIndicatorBOLL;
    self.tdBtn.tag = KKIndicatorTD;

    self.macdBtn.tag = KKIndicatorMACD;
    self.kdjBtn.tag = KKIndicatorKDJ;
    self.rsiBtn.tag = KKIndicatorRSI;
    self.wrBtn.tag = KKIndicatorWR;
}

- (void)setBtnArrs{
    self.mainBtnArr = @[self.maBtn,self.emaBtn,self.bollBtn,self.tdBtn];
    self.subBtnArr = @[self.macdBtn,self.kdjBtn,self.rsiBtn,self.wrBtn];
    self.selectArr = [[NSMutableArray alloc] init];
    
    //设置main 按钮初始值是否选中
    NSInteger mainType = [[TabbarSelectUtil share] getTabbarIndicatorMainBtnTag];
    for (UIButton *btn in self.mainBtnArr){
        if (mainType == btn.tag){
            btn.selected = YES;
            [btn setBackgroundColor:INDICATOR_BUTTON_SELECTED_BACKGROUND_COLOR];
            self.mainHideBtn.selected = NO;
        }
    }
    
    //设置sub 按钮初始值是否选中
    NSArray *subArray = [[TabbarSelectUtil share] getTabbarIndicatorSubBtnTag];
    if (subArray.count > 0) {
        for (UIButton *btn in self.subBtnArr){
            for (int i = 0; i < subArray.count; i++) {
                NSString *subType = [subArray objectAtIndex:i];
                if (subType.integerValue == btn.tag){
                    [self.selectArr addObject:@(btn.tag)];
                    btn.selected = YES;
                    [btn setBackgroundColor:INDICATOR_BUTTON_SELECTED_BACKGROUND_COLOR];
                    break;
                }
            }
        }
        self.sideHideBtn.selected = NO;
    }
}

#pragma mark - set up label text

- (void)setUpData {
    self.mainLabel.text = [[KKCryptoChartDataManager shareManager] getLocalizableStringWithKey:@"chart_main"];
    self.subLabel.text = [[KKCryptoChartDataManager shareManager] getLocalizableStringWithKey:@"chart_sub"];
}

#pragma mark - Main Indicator Actions

- (IBAction)maBtnSelected:(id)sender {
    [self changeMainBtnStatus:(UIButton *)sender];
}
- (IBAction)emaBtnSelected:(id)sender {
    [self changeMainBtnStatus:(UIButton *)sender];
}
- (IBAction)bollBtnSelected:(id)sender {
    [self changeMainBtnStatus:(UIButton *)sender];
}
- (IBAction)tdBtnSelected:(id)sender {
    [self changeMainBtnStatus:(UIButton *)sender];
}
- (IBAction)mainHideBtnSelected:(id)sender {
    if (!self.mainHideBtn.selected) {
        [self setAllMainBtnUnselected];
        //移除main的按钮选中记录
        [[TabbarSelectUtil share]removeTabbarIndicatorMainBtnTag];
        self.mainHideBtn.selected = YES;
        if ([self.delegate respondsToSelector:@selector(indicatorSelectorMainChartValueChanged:)]) {
            [self.delegate indicatorSelectorMainChartValueChanged:KKIndicatorNone];
        }
    }
}

- (void)changeMainBtnStatus:(UIButton *)btn {
    KKLog(@"KKIndicatorSelector changeMainBtnStatus btn tag=%lu", (unsigned long)btn.tag);
    BOOL isSelected = btn.selected;
    [self setAllMainBtnUnselected];
    [btn setSelected:!isSelected];
    [btn setBackgroundColor:btn.selected ? INDICATOR_BUTTON_SELECTED_BACKGROUND_COLOR : INDICATOR_BUTTON_DEFAULT_BACKGROUND_COLOR];
    if (!isSelected) {
        //记录main的按钮选中
        [[TabbarSelectUtil share]setTabbarIndicatorMainBtnTagWithTag:btn.tag];
        if ([self.delegate respondsToSelector:@selector(indicatorSelectorMainChartValueChanged:)]) {
            [self.delegate indicatorSelectorMainChartValueChanged:btn.tag];;
        }
        self.mainHideBtn.selected = NO;
    } else {
        //移除main的按钮选中记录
        [[TabbarSelectUtil share]removeTabbarIndicatorMainBtnTag];
        if ([self.delegate respondsToSelector:@selector(indicatorSelectorMainChartValueChanged:)]) {
            [self.delegate indicatorSelectorMainChartValueChanged:KKIndicatorNone];
        }
        self.mainHideBtn.selected = YES;
    }
}

- (void)setAllMainBtnUnselected {
    [self.maBtn setSelected:NO];
    [self.maBtn setBackgroundColor:INDICATOR_BUTTON_DEFAULT_BACKGROUND_COLOR];
    [self.emaBtn setSelected:NO];
    [self.emaBtn setBackgroundColor:INDICATOR_BUTTON_DEFAULT_BACKGROUND_COLOR];
    [self.bollBtn setSelected:NO];
    [self.bollBtn setBackgroundColor:INDICATOR_BUTTON_DEFAULT_BACKGROUND_COLOR];
    [self.tdBtn setSelected:NO];
    [self.tdBtn setBackgroundColor:INDICATOR_BUTTON_DEFAULT_BACKGROUND_COLOR];
}

#pragma mark - Side Indicator Actions

- (IBAction)macdBtnSelected:(id)sender {
    [self changeSideBtnStatus:(UIButton *)sender];
}
- (IBAction)kdjBtnSelected:(id)sender {
    [self changeSideBtnStatus:(UIButton *)sender];
}
- (IBAction)rsiBtnSelected:(id)sender {
    [self changeSideBtnStatus:(UIButton *)sender];
}
- (IBAction)wrBtnSelected:(id)sender {
    [self changeSideBtnStatus:(UIButton *)sender];
}
- (IBAction)sideHideBtnSelected:(id)sender {
    if (!self.sideHideBtn.selected) {
        [self setAllSideBtnUnselected];
        //移除sub的按钮选中记录
        [[TabbarSelectUtil share]removeTabbarIndicatorSubBtnTag];
        [self.selectArr removeAllObjects];
        self.sideHideBtn.selected = YES;
        if ([self.delegate respondsToSelector:@selector(indicatorSelectorSideChartValueChanged:)]) {
            [self.delegate indicatorSelectorSideChartValueChanged:self.selectArr];
        }
    }
}

- (void)setAllSideBtnUnselected {
    [self.macdBtn setSelected:NO];
    [self.macdBtn setBackgroundColor:INDICATOR_BUTTON_DEFAULT_BACKGROUND_COLOR];
    [self.kdjBtn setSelected:NO];
    [self.kdjBtn setBackgroundColor:INDICATOR_BUTTON_DEFAULT_BACKGROUND_COLOR];
    [self.rsiBtn setSelected:NO];
    [self.rsiBtn setBackgroundColor:INDICATOR_BUTTON_DEFAULT_BACKGROUND_COLOR];
    [self.wrBtn setSelected:NO];
    [self.wrBtn setBackgroundColor:INDICATOR_BUTTON_DEFAULT_BACKGROUND_COLOR];
}

- (void)changeSideBtnStatus:(UIButton *)btn {
    
    BOOL isSelected = btn.selected;
    [btn setSelected:!isSelected];
    [btn setBackgroundColor:btn.selected ? INDICATOR_BUTTON_SELECTED_BACKGROUND_COLOR : INDICATOR_BUTTON_DEFAULT_BACKGROUND_COLOR];
    if (!isSelected) {
        //记录sub的按钮选中
        [self.selectArr addObject:@(btn.tag)];
        [[TabbarSelectUtil share]setTabbarIndicatorSubBtnTagWithTag:self.selectArr];
        if ([self.delegate respondsToSelector:@selector(indicatorSelectorSideChartValueChanged:)]) {
            [self.delegate indicatorSelectorSideChartValueChanged:self.selectArr];
        }
    } else {
        //移除sub的按钮选中记录
        [self.selectArr removeObject:@(btn.tag)];
        [[TabbarSelectUtil share]setTabbarIndicatorSubBtnTagWithTag:self.selectArr];
        if ([self.delegate respondsToSelector:@selector(indicatorSelectorSideChartValueChanged:)]) {
            [self.delegate indicatorSelectorSideChartValueChanged:self.selectArr];
        }
    }
    
    if (self.selectArr.count > 0) {
        self.sideHideBtn.selected = NO;
    } else {
        self.sideHideBtn.selected = YES;
    }
}

@end
