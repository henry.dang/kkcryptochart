//
//  KKKLineBaseApi.m
//  KikiChartsDemo
//
//  Created by apple on 2021/3/3.
//

#import "KKKLineBaseApi.h"

NSString *const BaseProdURL = @"https://api.kikitrade.com/v1/market/klines/init";
NSString *const BaseBetaURL = @"http://api.beta.dipbit.xyz/v1/market/klines/init";

@interface KKKLineBaseApi()

@property (nonatomic, strong) NSString *baseUrl;

@end

@implementation KKKLineBaseApi

- (void)setEnvironment:(KKCryptoChartEnvironmentType)environment {
    _environment = environment;
    switch (environment) {
        case KKCryptoChartEnvironmentTypeBeta:
            _baseUrl = BaseBetaURL;
            break;
        case KKCryptoChartEnvironmentTypeProduct:
        default:
            _baseUrl = BaseProdURL;
            break;
    }
}

- (NSString *)getBaseURL {
    return self.baseUrl;
}

- (NSString *)getCustomURL {
    return nil;
}

- (NSString *)getCompletedURL {
    return nil;
}

@end
