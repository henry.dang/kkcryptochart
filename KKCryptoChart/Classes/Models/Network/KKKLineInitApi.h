//
//  KKKLineInitApi.h
//  KikiChartsDemo
//
//  Created by apple on 2021/3/3.
//

#import "KKKLineBaseApi.h"

NS_ASSUME_NONNULL_BEGIN

@interface KKKLineInitApi : KKKLineBaseApi

// 盘口名
@property (nonatomic, strong) NSString *symbol;

// 时间维度
@property (nonatomic, strong) NSString *resolution;

// 起始时间戳
@property (nonatomic, strong) NSString *from;

// 结束时间戳
@property (nonatomic, strong) NSString *to;

@end

NS_ASSUME_NONNULL_END
