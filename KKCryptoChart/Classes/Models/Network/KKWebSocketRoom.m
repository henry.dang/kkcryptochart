//
//  KKWebSocketRoom.m
//  KKCryptoChart
//
//  Created by Henry on 2022/2/24.
//

#import "KKWebSocketRoom.h"

@implementation KKWebSocketRoom

-(instancetype)initWithRoom:(NSString *)room symbol:(NSString *)symbol fromSymbol:(NSString *)fromSymbol resolution:(NSString *)resolution {
    if (self = [super init]) {
        _room = room;
        _symbol = symbol;
        _fromSymbol = fromSymbol;
        _resolution = resolution;
    }
    return self;
}

@end
