//
//  KKWebSocketConfig.h
//  KKCryptoChart
//
//  Created by Henry on 2022/2/24.
//

#import <Foundation/Foundation.h>

@interface KKWebSocketConfig : NSObject

@property(nonatomic, strong) NSString *authType;
@property(nonatomic, strong) NSString *appCode;
@property(nonatomic, strong) NSString *contentType;
@property(nonatomic, strong) NSString *acceptType;

-(instancetype)initWithAuthType:(NSString *)authType appCode:(NSString *)appCode contentType:(NSString *)contentType acceptType:(NSString *)acceptType;

@end

