//
//  KKCryptoChartDataManager.h
//  KikiChartsDemo
//
//  Created by apple on 2021/3/4.
//

#import <Foundation/Foundation.h>
#import "KKCryptoChartGlobalConfig.h"
#import "KKIndicatorModel.h"

// 时间段
typedef NS_ENUM(NSInteger, KKCryptoChartTimeType) {
    KKCryptoChartTimeTypeOneMinute = 100, // 1
    KKCryptoChartTimeTypeFiveMinutes, // 5
    KKCryptoChartTimeTypeFifteenMinutes, // 15
    KKCryptoChartTimeTypeThirtyMinutes, // 30
    KKCryptoChartTimeTypeOneHour, // 60
    KKCryptoChartTimeTypeFourHours,  // 240
    KKCryptoChartTimeTypeOneDay,  // D
    KKCryptoChartTimeTypeOneWeek,  // W
    KKCryptoChartTimeTypeOneMonth,  // M
    KKCryptoChartTimeTypeOneYear // 12M
};

NS_ASSUME_NONNULL_BEGIN

@class KKCryptoChartModel;
@interface KKCryptoChartDataManager : NSObject

@property (nonatomic, strong) KKCryptoChartGlobalConfig *config;

+(instancetype)shareManager;

/**
 get localizable string from bundle
 */
- (NSString *)getLocalizableStringWithKey:(NSString *)key;

/**
 * fetch cloud data from Kiki api
 */
- (void)fetchCloudDataWithCoinCode:(NSString *)coinCode timeType:(NSString *)timeType environment:(KKCryptoChartEnvironmentType)envType completionHandler:(void (^)(NSArray<KKCryptoChartModel *> *chartModels, NSString *error))completionHandler;
- (KKCryptoChartTimeType)getTimeTypeEnum:(NSString *)timeType;
- (NSString *)getTimeString:(KKCryptoChartTimeType)timeType;

/**
  get indicator cycles eg: ma-(7,25,99)
 */
- (nonnull NSArray *)getIndicatorTimeCycles:(KKIndicator)indicatorType;

/**
 * get web socket resolution eg:@"P1m"
 */
- (NSString *)getSocketResolution:(KKCryptoChartTimeType)timeType;

/**
 *get time string eg: @"p1m" -> @"1"
 */
- (NSString *)getTimeStringFromSocketResolution:(NSString *)resolution;

/**
 * calculate indicaotor data
 */
- (void)calculateIndicatorData:(NSArray<KKCryptoChartModel *> *)candleDatas;

@end

NS_ASSUME_NONNULL_END
