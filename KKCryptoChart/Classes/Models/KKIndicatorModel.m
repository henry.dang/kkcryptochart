//
//  KKIndicatorModel.m
//  KikiChartsDemo
//
//  Created by apple on 2021/3/2.
//

#import "KKIndicatorModel.h"
#import "KKCryptoChartModel.h"
#import <CoreFoundation/CoreFoundation.h>
#import <UIKit/UIKit.h>

@implementation KKMACDModel

+ (double)emaWithLastEma:(double)lastEma close:(double)close n:(int)n {
    double a = 2.0 / (float)(n + 1);
    return a * close + (1 - a) * lastEma;
}

+ (double)deaWithLasDea: (double)lasDea curDiff:(double)curDiff {
    return [self deaWithLasDea:lasDea curDiff:curDiff day:9];
}

+ (double)deaWithLasDea: (double)lasDea curDiff:(double)curDiff day:(int)n {
    double a = 2.0 / (float)(n + 1);
    return a * curDiff + (1 - a) * lasDea;
}

/**
 * 12, 26, 9
 *
 * 计算macd指标,快速和慢速移动平均线的周期分别取12和26
 *
 * @method MACD
 * @param datas 计算数据
 *
 */
+ (void)calMACDWithData:(NSArray<KKCryptoChartModel *>*)datas params:(NSArray *)params {
    if ([params count] != 3) {
        return;
    }
    NSMutableArray *ema12 = @[].mutableCopy;
    NSMutableArray *ema26 = @[].mutableCopy;
    NSMutableArray *diffs = @[].mutableCopy;
    NSMutableArray *deas = @[].mutableCopy;
    NSMutableArray *bars = @[].mutableCopy;
    
    int p1 = [params[0] intValue];
    int p2 = [params[1] intValue];
    int p3 = [params[2] intValue];

    for (int i = 0; i < datas.count; i ++) {
        KKCryptoChartModel *t = datas[i];
        double c = t.close.doubleValue;
        if (i == 0) {
            [ema12 addObject: @(c)];
            [ema26 addObject: @(c)];
            [deas addObject: @(0)];
        } else {
            [ema12 addObject: @([self emaWithLastEma:[ema12[i - 1] doubleValue] close:c n:p1])];
            [ema26 addObject: @([self emaWithLastEma:[ema26[i - 1] doubleValue] close:c n:p2])];
        }
        [diffs addObject:@(([ema12[i] doubleValue] - [ema26[i] doubleValue]))];
        if (i != 0) {
            [deas addObject:@([self deaWithLasDea:[deas[i - 1] doubleValue] curDiff:[diffs[i] doubleValue] day:p3])];
        }
        [bars addObject:@(([diffs[i] doubleValue] - [deas[i] doubleValue]) * 2)];
    }
    [datas enumerateObjectsUsingBlock:^(KKCryptoChartModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        KKMACDModel *m = [KKMACDModel new];
        if (idx >= p2 - 1) {
            m.DIFF = diffs[idx];//26起点
        } else {
            m.DIFF = [NSNumber numberWithFloat:0];
        }

        if (idx >= p2 + p3 - 1) {
            m.DEA = deas[idx];//9+26起点
            m.MACD = bars[idx];//9+26起点
        } else {
            m.DEA = [NSNumber numberWithFloat:0];
            m.MACD = [NSNumber numberWithFloat:0];
        }
        obj.MACD = m;
    }];
}
@end

@implementation KKKDJModel
/**
 * 9,3,3
 * 计算kdj指标,rsv的周期为9日
 *
 * @method KDJ
 * @param datas datas
 * 二维数组类型，其中内层数组包含三个元素值，第一个值表示当前Tick的最高价格，第二个表示当前Tick的最低价格，第三个表示当前Tick的收盘价格
 */
+ (void)calKDJWithData:(NSArray<KKCryptoChartModel *>*)datas params:(NSArray *)params {
    if ([params count] != 3) {
        return;
    }
    
    NSMutableArray<KKCryptoChartModel*> *nineDaysdatas = @[].mutableCopy;
    int days = [params[0] intValue];
    double m1 =  1.0 / [params[1] doubleValue];
    double m2 = 1.0 / [params[2] doubleValue];
    
    NSMutableArray<NSNumber *> *rsvs = @[].mutableCopy;
    NSMutableArray<NSNumber *> *ks = @[].mutableCopy;
    NSMutableArray<NSNumber *> *ds = @[].mutableCopy;
    NSMutableArray<NSNumber *> *js = @[].mutableCopy;

    for (int i = 0; i < datas.count; i ++) {
        KKCryptoChartModel *t = datas[i];
        double c = [t.close doubleValue];
        [nineDaysdatas addObject:t];
        double max = 0;
        double min = INFINITY;
        for (KKCryptoChartModel *mn in nineDaysdatas) {
            max = MAX(mn.high.doubleValue, max);
            min = MIN(mn.low.doubleValue, min);
        }
        if (max == min) {
            [rsvs addObject:@(0)];
        } else {
            double v = ((c - min) / (max - min) * 100.0);
            [rsvs addObject:@(v)];
        }
        if (nineDaysdatas.count == days) {
            [nineDaysdatas removeObjectAtIndex:0];
        }
        if (i == 0) {
            double k = [rsvs[i] doubleValue];
            [ks addObject:@(k)];
            double d = k;
            [ds addObject:@(d)];
            double j = 3.0 * k - 2.0 * d;
            [js addObject:@(j)];
        } else {
            double k = (1 - m1) * [ks[i - 1] doubleValue] + m1 * [rsvs[i] doubleValue];
            [ks addObject:@(k)];
            double d = (1- m2) * [ds[i - 1] doubleValue] + m2 * k;
            [ds addObject:@(d)];
            double j = 3.0 * k - 2.0 * d;
            [js addObject:@(j)];
        }
    }
    [datas enumerateObjectsUsingBlock:^(KKCryptoChartModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        KKKDJModel *m = [KKKDJModel new];
        if (idx >= days - 1) {
            m.K = ks[idx];
            m.D = ds[idx];
            m.J = js[idx];
            m.RSV = rsvs[idx];
        } else {
            m.K = [NSNumber numberWithFloat:0];
            m.D = [NSNumber numberWithFloat:0];
            m.J = [NSNumber numberWithFloat:0];
            m.RSV = [NSNumber numberWithFloat:0];
        }
        obj.KDJ = m;
    }];
}
@end

@implementation KKMAModel
/**
 * 计算移动平均线指标, ma的周期为days
 
 @param datas 数据
 @param params MA计算参数
 */
+ (void)calMAWithData:(NSArray<KKCryptoChartModel *>*)datas params:(NSArray *)params {
    if ([params count] == 0) {
        return ;
    }
    NSArray *days = params;
    NSMutableDictionary *result = @{}.mutableCopy;
    NSMutableDictionary *paramData = @{}.mutableCopy;
    for (int i = 0; i < datas.count; i ++) {
        KKCryptoChartModel *t = datas[i];
        double c = [t.close doubleValue];
        
        for (NSString *d in days) {
            NSString *ma = d;
            if ([result objectForKey:ma] == nil) {
                result[ma] = @[].mutableCopy;
            }
            if ([paramData objectForKey:ma] == nil) {
                paramData[ma] = @[].mutableCopy;
            }
            
            NSMutableArray *mas = result[ma];
            NSMutableArray *nma = paramData[ma];
            [nma addObject:@(c)];
            
            if (nma.count == d.integerValue) {
                double nowMa = 0.0;
                for (NSNumber *n in nma) {
                    nowMa += [n doubleValue];
                }
                nowMa = nowMa / [d doubleValue];
                [mas addObject:@(nowMa)];
                [nma removeObjectAtIndex:0];
            } else {
                double nowMa = 0.0;
                for (NSNumber *n in nma) {
                    nowMa += [n doubleValue];
                }
                nowMa = nowMa / (float)nma.count;
                [mas addObject:@(nowMa)];
            }
        }
    }
    [datas enumerateObjectsUsingBlock:^(KKCryptoChartModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        KKMAModel *m = [KKMAModel new];
        m.MA1 = result[params[0]][idx];
        m.MA2 = result[params[1]][idx];
        m.MA3 = result[params[2]][idx];
        obj.MA = m;
    }];
}
@end

@implementation KKRSIModel
/**
 *
 * 计算rsi指标,分别返回以6日，12日，24日为参考基期的RSI值
 *
 * @method RSI
 * @param datas
 * 一维数组类型，每个元素为当前Tick的收盘价格
 */

+ (void)calRSIWithData:(NSArray<KKCryptoChartModel *>*)datas params:(NSArray *)params {
    if ([params count] == 0) {
        return;
    }
    double lastClosePx = datas[0].close.floatValue;
    NSArray *days = params;
    NSMutableDictionary *result = @{}.mutableCopy;
    
    for (int i = 0; i < datas.count; i ++) {
        KKCryptoChartModel *t = datas[i];
        double c = t.close.floatValue;
        
        double m = MAX(c - lastClosePx, 0);
        double a = fabs(c - lastClosePx);
       
        for (NSString *d in days) {
            NSString *lastSm = [NSString stringWithFormat:@"lastSm%@", d];
            NSString *lastSa = [NSString stringWithFormat:@"lastSa%@", d];
            NSString *rsi = [NSString stringWithFormat:@"rsi%@", d];
            if ([result objectForKey:rsi] == nil) {
                result[lastSm] = @(0);
                result[lastSa]  = @(0);
                result[rsi] = @[@(0)].mutableCopy;
            } else {
                result[lastSm] = @((m + ([d doubleValue] - 1) * [result[lastSm] doubleValue]) / [d doubleValue]);
                result[lastSa] = @((a + ([d doubleValue] - 1) * [result[lastSa] doubleValue]) / [d doubleValue]);
                NSMutableArray *ary = result[rsi];
                if ([result[lastSa] doubleValue] != 0) {
                    [ary addObject:@([result[lastSm] doubleValue] / [result[lastSa] doubleValue] * 100.0)];
                } else {
                    [ary addObject:@(0)];
                }
            }
        }
        lastClosePx = c;
    }
    [datas enumerateObjectsUsingBlock:^(KKCryptoChartModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        KKRSIModel *m = [KKRSIModel new];
        if (idx >= [days[0] intValue] - 1) {
            m.RSI1 = result[[NSString stringWithFormat:@"rsi%@", params[0]]][idx];
        } else {
            m.RSI1 = [NSNumber numberWithFloat:0];
        }
        
        if (idx >= [days[1] intValue] - 1) {
            m.RSI2 = result[[NSString stringWithFormat:@"rsi%@", params[1]]][idx];
        } else {
            m.RSI2 = [NSNumber numberWithFloat:0];
        }
        
        if (idx >= [days[2] intValue] - 1) {
            m.RSI3 = result[[NSString stringWithFormat:@"rsi%@", params[2]]][idx];
        } else {
            m.RSI3 = [NSNumber numberWithFloat:0];
        }
        obj.RSI = m;
    }];
}
@end

@implementation KKBOLLModel
/**
 *
 * 计算boll指标,ma的周期为20日
 *
 * @method BOLL
 * @param datas datas
 * 一维数组类型，每个元素为当前Tick的收盘价格
 */
+ (void)calBOLLWithData:(NSArray<KKCryptoChartModel *>*)datas params:(NSArray *)params {
    if ([params count] != 2) {
        return;
    }
    
    int maDays = [params[0] intValue];
    int k = [params[1] intValue];;
    NSMutableArray<NSNumber *> *ups = @[].mutableCopy;
    NSMutableArray<NSNumber *> *mas = @[].mutableCopy;
    NSMutableArray<NSNumber *> *lows = @[].mutableCopy;
NSMutableArray *nma = @[].mutableCopy;

    //移动平均线周期为21
    for (int i = 0; i < datas.count; i ++) {
        KKCryptoChartModel *t = datas[i];
        double c = t.close.floatValue;
        [nma addObject:@(c)];
        if (nma.count == maDays) {
            NSNumber *mb = mas[i -1];
            double nowMa = 0.0;
            double sumMD = 0.0;
            for (NSNumber *n in nma) {
                nowMa += [n doubleValue];
                sumMD += pow([n doubleValue] - [mb doubleValue], 2);
            }
            nowMa = nowMa / (float)maDays;
            [mas addObject:@(nowMa)];
            [nma removeObjectAtIndex:0];
            
            double md = sqrt(sumMD / maDays);
            double up = [mb doubleValue] + k * md;
            double dn = [mb doubleValue] - k * md;
            [ups addObject:@(up)];
            [lows addObject:@(dn)];

        } else {
            [mas addObject:@(c)];
            [ups addObject:@(c)];
            [lows addObject:@(c)];
        }
    }
    [datas enumerateObjectsUsingBlock:^(KKCryptoChartModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        KKBOLLModel *m = [KKBOLLModel new];
        m.UP = ups[idx];
        m.MID = mas[idx];
        m.LOW = lows[idx];
        obj.BOLL = m;
    }];
}
@end

@implementation KKWRModel
/**
 *
 * 计算wr指标,分别返回以6日，10日，参考基期的WR值
 *
 * @method WR
 * @param datas
 * 一维数组类型，每个元素为当前Tick的收盘价格
 */

+ (void)calWRWithData:(NSArray<KKCryptoChartModel *>*)datas params:(NSArray *)params {
    if ([params count] == 0) {
        return;
    }
    NSArray *days = params;
    NSMutableDictionary *paramData = @{}.mutableCopy;
    NSMutableDictionary *result = @{}.mutableCopy;
    for (int i = 0; i < datas.count; i ++) {
        KKCryptoChartModel *t = datas[i];
        double c = t.close.floatValue;
        for (NSString *d in days) {
            NSString *wr = [NSString stringWithFormat:@"wr%@", d];
            if ([result objectForKey:wr] == nil) {
                result[wr] = @[].mutableCopy;
            }
            if ([paramData objectForKey:wr] == nil) {
                paramData[wr] = @[].mutableCopy;
            }
            NSMutableArray *wrs = result[wr];
            NSMutableArray *nwr = paramData[wr];
            
            [nwr addObject:t];
            double max = 0;
            double min = INFINITY;
            for (KKCryptoChartModel *mn in nwr) {
                max = MAX(mn.high.floatValue, max);
                min = MIN(mn.low.floatValue, min);
            }
            double nowWR = max > min ? 100.0 * (max - c) / (max - min) : 100;
            [wrs addObject:@(nowWR)];
            if (nwr.count == d.integerValue) {
                [nwr removeObjectAtIndex:0];
            }
        }
    }
    
    [datas enumerateObjectsUsingBlock:^(KKCryptoChartModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        KKWRModel *m = [KKWRModel new];
        if (idx >= [days[0] intValue] - 1) {
            m.WR1 = result[[NSString stringWithFormat:@"wr%@", params[0]]][idx];
        } else {
            m.WR1 = [NSNumber numberWithFloat:0];
        }

        if (idx >= [days[1] intValue] - 1) {
            m.WR2 = result[[NSString stringWithFormat:@"wr%@", params[1]]][idx];
        } else {
            m.WR2 = [NSNumber numberWithFloat:0];
        }
        
        obj.WR = m;
    }];
}
@end

@implementation KKEMAModel

+ (double)emaWithLastEma:(double)lastEma close:(double)close n:(int)n {
    double a = 2.0 / (float)(n + 1);
    return a * close + (1 - a) * lastEma;
}

/**
*  EMA(n) = 2 / (n + 1) * (C(n) - EMA(n - 1)) + EMA(n - 1)
*  C(n):本期收盘价格
*/
+ (void)calEmaWithData:(NSArray<KKCryptoChartModel *>*)datas params:(NSArray *)params {
    if (params.count == 0) {
        return;
    }
    NSArray *days = params;
    NSMutableDictionary *resultDict = [NSMutableDictionary dictionary];
    for (int i = 0; i < datas.count; i ++) {
        KKCryptoChartModel *model = datas[i];
        CGFloat close = model.close.floatValue;
        for (NSString *day in days) {
            NSString *key = [NSString stringWithFormat:@"ema%@", day];
            if ([resultDict objectForKey:key] == nil) {
                resultDict[key] = [NSMutableArray array];
            }
            NSMutableArray *emaArray = resultDict[key];
            if (i == 0) {
                [emaArray addObject:@(close)];
            } else {
                [emaArray addObject:@([self emaWithLastEma:[emaArray[i - 1] doubleValue] close:close n:[day intValue]])];
            }
        }
    }
    [datas enumerateObjectsUsingBlock:^(KKCryptoChartModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        KKEMAModel *m = [KKEMAModel new];
        m.EMA1 = resultDict[[NSString stringWithFormat:@"ema%@", params[0]]][idx];
        m.EMA2 = resultDict[[NSString stringWithFormat:@"ema%@", params[1]]][idx];
        m.EMA3 = resultDict[[NSString stringWithFormat:@"ema%@", params[2]]][idx];
        obj.EMA = m;
    }];
}
@end

@implementation KKTDModel

+ (void)calTDWithData:(NSArray<KKCryptoChartModel *> *)datas {
    if (!datas || datas.count == 0) {
        return;
    }

    NSMutableArray<KKTDModel *> *tds = [NSMutableArray array];
    for (int i = 0; i < datas.count; i++) {
        KKTDModel *td = [[KKTDModel alloc] init];

        if (i >= 5) {
            td.sellCountdownIndex = tds[i - 1].sellCountdownIndex;
            td.buyCountdownIndex = tds[i - 1].buyCountdownIndex;
            td.sellSetup = tds[i - 1].sellSetup;
            td.buySetup = tds[i - 1].buySetup;
            td.TDSTBuy = tds[i - 1].TDSTBuy;
            td.TDSTSell = tds[i - 1].TDSTSell;
            td.sellSetupPerfection = tds[i - 1].sellSetupPerfection;
            td.buySetupPerfection = tds[i - 1].buySetupPerfection;
            
            BOOL closeLessThanCloseOf4BarsEarlier = datas[i].close.doubleValue < datas[i - 4].close.doubleValue;
            BOOL closeGreaterThanCloseOf4BarsEarlier = datas[i].close.doubleValue > datas[i - 4].close.doubleValue;
            
            // Bearish Price Flip - a close greater than the close four bars earlier, immediately followed by a close less than the close four bars earlier.
            // Bullish Price Flip -  a close less than the close four bars before, immediately followed by a close greater than the close four bars earlier.
            td.bearishFlip = datas[i - 1].close.doubleValue > datas[i - 5].close.doubleValue && closeLessThanCloseOf4BarsEarlier;
            td.bullishFlip = datas[i - 1].close.doubleValue < datas[i - 5].close.doubleValue && closeGreaterThanCloseOf4BarsEarlier;

            //TD Buy Setup -  a bearish price flip, which indicates a switch from positive to negative momentum.
            // – After a bearish price flip, there must be nine consecutive closes, each one less than the corresponding close four bars earlier.
            // – Cancellation - If at any point a bar closes higher than the close four bars earlier the setup is canceled and we are waiting for another price flip
            // - Setup perfection – the low of bars 8 or 9 should be lower than the low of bar 6 and bar 7 (if not satisfied expect new low/retest of the low).
            
            // TD buySetup
            if (td.bearishFlip || (tds[i - 1].buySetupIndex > 0 && closeLessThanCloseOf4BarsEarlier)) {
              td.buySetupIndex = [NSNumber numberWithInt:(tds[i - 1].buySetupIndex.integerValue + 1 - 1) % 13 + 1];
                td.TDSTBuy = [NSNumber numberWithDouble:MAX(datas[i].high.doubleValue, tds[i - 1].TDSTBuy.doubleValue)];
            } else if (td.bullishFlip || (tds[i - 1].sellSetupIndex > 0 && closeGreaterThanCloseOf4BarsEarlier)) {
              td.sellSetupIndex = [NSNumber numberWithInt:(tds[i - 1].sellSetupIndex.integerValue + 1 - 1) % 13 + 1];
              td.TDSTSell = [NSNumber numberWithDouble:MAX(datas[i].low.doubleValue, tds[i - 1].TDSTSell.doubleValue)];
            }
            
            // Did buy setup happen?
            if (td.buySetupIndex.intValue == 13) {
              td.buySetup = true;
              td.sellSetup = false;
              td.sellSetupPerfection = false;

              // - Buy Setup perfection – the low of bars 8 or 9 should be lower than the low of bar 6 and bar 7 (if not satisfied expect new low/retest of the low).
              td.buySetupPerfection =
                (datas[i - 1].low.doubleValue < datas[i - 3].low.doubleValue && datas[i - 1].low.doubleValue < datas[i - 2].low.doubleValue) ||
                // bar 9 low < 6 and 7
                (datas[i].low.doubleValue < datas[i - 3].low.doubleValue && datas[i].low.doubleValue < datas[i - 2].low.doubleValue);
            }

            // Did sell setup happen?
            if (td.sellSetupIndex.intValue == 13) {
              td.sellSetup = true;
              td.buySetup = false;
              td.buySetupPerfection = false;

              // - Sell Setup perfection – the high of bars 8 or 9 should be greater than the high of bar 6 and bar 7 (if not satisfied expect new high/retest of the high).

              td.sellSetupPerfection =
                (datas[i - 1].high.doubleValue > datas[i - 3].high.doubleValue && datas[i - 1].high.doubleValue > datas[i - 2].high.doubleValue) ||
                // bar 9 high > 6 and 7
                (datas[i].high.doubleValue > datas[i - 3].high.doubleValue && datas[i].high.doubleValue > datas[i - 2].high.doubleValue);
            }
            
            // TD Countdown compares the current close with the low/high two bars earlier and you count 13 bars.
            // TD Buy Countdown
            // starts after the finish of a buy setup.
            // The close of bar 9 should be "less" than the low two bars earlier. If satisfied bar 9 of the setup becomes bar 1 of the countdown. If the condition is not met than bar 1 of the countdown is postponed until the conditions is satisfied and you continue to count until there are a total of thirteen closes, each one less than, or equal to, the low two bars earlier.
            // Countdown qualifier - The low of Countdown bar thirteen must be less than, or equal to, the close of Countdown bar eight.
            // Countdown cancellation:
            // - A sell Setup appears. The price has rallied in the opposite direction and the market dynamic has changed.
            // - close above the highest high for the current buy Setup (break of TDST for the current Setup)
            // - recycle occurs ( new Setup in the same direction and recycle activated )

            // Setup Recycle - A second setup appears in the same direction before/on/after a Countdown - that usually means strength. The question is recycle and start a new Countdown? (there must be a price flip to divide the two setups or the first just continuous)
            // Compare the size of the two setups. The size is the difference between the true high and true low of a setup.
            // - if the second setup is equal or greater than the previous one , but less than 1.618 times its size, then Setup recycle will occur – the trend re-energize itself. Whichever Setup has the larger true range will become the active Setup.
            // - ignore setup recycle if the new setup is smaller or 1.618 times and more, bigger than the previous one – most probably price exhaustion area.
            [self calculateTDBuyCountdown:tds resultObj:td ohlc:datas item:datas[i] i:i];
            [self calculateTDSellCountdown:tds resultObj:td ohlc:datas item:datas[i] i:i];
        }
        [tds addObject:td];
    }
    if (tds.count == datas.count) {
        [datas enumerateObjectsUsingBlock:^(KKCryptoChartModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            obj.TD = tds[idx];
        }];
    }
}

+ (void)calculateTDBuyCountdown:(NSMutableArray<KKTDModel *> *)result
                       resultObj:(KKTDModel *)resultObj
                            ohlc:(NSArray<KKCryptoChartModel *> *)ohlc
                            item:(KKCryptoChartModel *)item
                               i:(int)i {
    //First we do cancellations:
    //If we were doing buy countdown, and now sell setup happens
    if (
      // Sell setup appears
      (result[i - 1].buySetup && resultObj.sellSetup) ||
      // Close above TDSTBuy
      (item.close.doubleValue > item.TD.TDSTBuy.doubleValue)
    ) {
      resultObj.buyCountdownIndex = @0;
      resultObj.countdownResetForTDST = true;
    } else if (resultObj.buySetup) {
      if (
        item.close.doubleValue < ohlc[i - 2].low.doubleValue
        // && item.close > ohlc[i - 1].low
      ) {
        resultObj.buyCountdownIndex = [NSNumber numberWithInt:(result[i - 1].buyCountdownIndex.integerValue + 1 - 1) % 13 + 1];
        resultObj.countdownIndexIsEqualToPreviousElement = false;
      }
    }

    //If this item and the preivous one were both 13, we set it to zero
    if (resultObj.buyCountdownIndex.integerValue == 13 && result[i - 1].buyCountdownIndex.integerValue == 13) {
      resultObj.buyCountdownIndex = @0;
    }

    //A.S: If coundown  hit 13, and we were counting another buy setup, we reset that buy setup
    if (resultObj.buyCountdownIndex.integerValue == 13 && resultObj.buySetupIndex.integerValue > 0) {
      resultObj.buySetupIndex = @1;
    }

    // If we just reset the countdown
    if (resultObj.buyCountdownIndex.integerValue != 13 && result[i - 1].buyCountdownIndex.integerValue == 13) {
      resultObj.buySetup = false;
      resultObj.buySetupPerfection = false;
      resultObj.buyCountdownIndex = @0;
    }
}


+ (void)calculateTDSellCountdown:(NSMutableArray<KKTDModel *> *)result
                      resultObj:(KKTDModel *)resultObj
                           ohlc:(NSArray<KKCryptoChartModel *> *)ohlc
                           item:(KKCryptoChartModel *)item
                              i:(int)i {
    // TD Sell countdown
    if (
      // Sell setup appears
      (result[i - 1].sellSetup && resultObj.buySetup) ||
      // Close above TDSTBuy
      (item.close.doubleValue < item.TD.TDSTSell.doubleValue)
    ) {
      resultObj.sellCountdownIndex = @0;
      resultObj.countdownResetForTDST = true;
    } else if (resultObj.sellSetup) {
      if (
        item.close > ohlc[i - 2].high
        // && item.close > ohlc[i - 1].high
      ) {
        resultObj.sellCountdownIndex = [NSNumber numberWithInt:(result[i - 1].sellCountdownIndex.integerValue + 1 - 1) % 13 + 1];
        resultObj.countdownIndexIsEqualToPreviousElement = false;
      }
    }

    //If this item and the preivous one were both 13, we set it to zero
    if (resultObj.sellCountdownIndex.integerValue == 13 && result[i - 1].sellCountdownIndex.integerValue == 13) {
      resultObj.sellCountdownIndex = @0;
    }

    //A.S: If coundown  hit 13, and we were counting another  setup, we reset that  setup
    if (resultObj.sellCountdownIndex.integerValue == 13 && resultObj.sellSetupIndex.integerValue > 0) {
      resultObj.sellSetupIndex = @1;
    }

    // If we just reset the countdown
    if (resultObj.sellCountdownIndex.integerValue != 13 && result[i - 1].sellCountdownIndex.integerValue == 13) {
      resultObj.sellSetup = false;
      resultObj.sellSetupPerfection = false;
      resultObj.sellCountdownIndex = @0;
    }
}

@end
